# gff2faldo

Convert [GFF3](https://m.ensembl.org/info/website/upload/gff3.html) data into [FALDO](https://github.com/OBF/FALDO) (tabulated or RDF)

# Requirements

- [gff2pandas](https://gitlab.com/odameron/gff2pandas)


# Todo

- [x] generate tabulated files
- [x] generate RDF files
    - [x] named graph
- [ ] generate metadata
    - [ ] VoID
