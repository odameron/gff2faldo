#! /usr/bin/env python3

import pandas
import pathlib
import gff2pandas

#gffPath = pathlib.Path('/home/olivier/projects/chromosome/ensembl/Homo_sapiens.GRCh38.93.chromosome.1-small.gff3')
gffPath = pathlib.Path('./example/sonicHedgehog.gff3')

#gffPattern = str(gffPath)[:gffPath.index('.gff')]
gffPattern = gffPath.parent / gffPath.stem

sequenceTermToIdent = {}
sequenceTermToIdent['chromosome'] = 'SO:0000340'
sequenceTermToIdent['biological_region'] = 'SO:0001411'
sequenceTermToIdent['mRNA'] = 'SO:0000234'
sequenceTermToIdent['exon'] = 'SO:0000147'

df = gff2pandas.parseGFF(gffPath)
df['strand'].fillna('.', inplace=True)
df['score'].fillna('.', inplace=True)
df['phase'].fillna('.', inplace=True)

print(df['type'].unique())
for currentType in df['type'].unique():
	with open("{}-{}.tsv".format(gffPattern, currentType), "w") as structFile:
		structFile.write("{}\t{}\n".format(currentType, "faldo:location@faldo:Region"))
	with open("{}-RegionPosition.tsv".format(gffPattern), "w") as structFile:
		structFile.write("faldo:Region\tfaldo:begin@faldo:ExactPosition\tfaldo:end@faldo:ExactPosition\n")
	with open("{}-Position.tsv".format(gffPattern), "w") as structFile:
		#structFile.write("faldo:ExactPosition\tfaldo:position@xsd:integer\trdf:type\n")
		structFile.write("faldo:ExactPosition\tfaldo:position\trdf:type\n")
with open("{}.ttl".format(gffPattern), "w") as rdfFile:
	rdfFile.write("""
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

@prefix faldo: <http://biohackathon.org/resource/faldo#> .
#@prefix so: <http://purl.obolibrary.org/obo/so#> .
@prefix so: <http://purl.obolibrary.org/obo/SO_> .
@prefix void: <http://rdfs.org/ns/void#> .

@prefix : <#> .

GRAPH <""" + gffPath.stem + """> {
""")


def getIdent(attrbField, prefixIdent="", defaultIdent=""):
	for attrbPair in attrbField.split(";"):
		(attrbKey, attrbValue) = attrbPair.split("=")
		if attrbKey == "ID":
			return prefixIdent + ':' + attrbValue.replace(':', '')
	return str(defaultIdent)

strand = {}
strand['.'] = ''
strand['+'] = 'faldo:ForwardStrandPosition'
strand['-'] = 'faldo:ReverseStrandPosition'

with open("{}.ttl".format(gffPattern), "a") as rdfFile:
	for index, row in df.iterrows():
		print("\n" + str(index))
		print(str(row))
		currentId = getIdent(row['attributes'], "", ":feature" + str(index))
		with open("{}-{}.tsv".format(gffPattern, row['type']), "a") as regionFile:
			regionFile.write("{}\t{}\n".format(currentId, currentId + "Region"))
		with open("{}-RegionPosition.tsv".format(gffPattern), "a") as regionPositionFile:
			regionPositionFile.write("{}\t{}\t{}\n".format(currentId + "Region", currentId + "RegionLocBegin", currentId + "RegionLocEnd"))
		with open("{}-Position.tsv".format(gffPattern), "a") as positionFile:
			positionFile.write("{}\t{}\t{}\n".format(currentId + "RegionLocBegin", row['start'], strand[row['strand']]))
			positionFile.write("{}\t{}\t{}\n".format(currentId + "RegionLocEnd", row['end'], strand[row['strand']]))
		rdfFile.write("\n")
		rdfFile.write("{} rdf:type {} .\n".format(currentId, ":" + row['type']))
		if row['type'] in sequenceTermToIdent.keys():
			rdfFile.write("{} rdf:type {} .\n".format(currentId, ":" + sequenceTermToIdent[row['type']]))
		rdfFile.write("{} faldo:localtion {} .\n".format(currentId, currentId + "Region"))
		rdfFile.write("{} rdf:type faldo:Region .\n".format(currentId + "Region"))
		rdfFile.write("{} faldo:begin {} .\n".format(currentId, currentId + "RegionLocBegin"))
		rdfFile.write("{} faldo:end {} .\n".format(currentId, currentId + "RegionLocEnd"))
		rdfFile.write("{} rdf:type faldo:ExactPosition .\n".format(currentId + "RegionLocBegin"))
		if row['strand'] == "+":
			rdfFile.write("{} rdf:type faldo:ForwardStrandPosition .\n".format(currentId + "RegionLocBegin"))
		elif row['strand'] == "-":
			rdfFile.write("{} rdf:type faldo:ReverseStrandPosition .\n".format(currentId + "RegionLocBegin"))
		rdfFile.write("{} faldo:position \"{}\"^^xsd:integer .\n".format(currentId + "RegionLocBegin", row['start']))
		rdfFile.write("{} rdf:type faldo:ExactPosition .\n".format(currentId + "RegionLocEnd"))
		if row['strand'] == "+":
			rdfFile.write("{} rdf:type faldo:ForwardStrandPosition .\n".format(currentId + "RegionLocEnd"))
		elif row['strand'] == "-":
			rdfFile.write("{} rdf:type faldo:ReverseStrandPosition .\n".format(currentId + "RegionLocEnd"))
		rdfFile.write("{} faldo:position \"{}\"^^xsd:integer .\n".format(currentId + "RegionLocEnd", row['end']))

	rdfFile.write("}")

